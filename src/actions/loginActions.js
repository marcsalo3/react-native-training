// import { signIn, signUp } from '../util/api';
//
// export const logOut = () => ({ type: 'log_out' });
// export const clearError = () => ({ type: 'clear_error' });
//
// export const createAccount = (email, password) => {
//   return (dispatch) => {
//     signUp(email, password)
//       .then((response) => {
//         const { client, token } = response;
//         dispatch({ type: 'access_account', token, client, email });
//       })
//       .catch((error) => {
//         dispatch({ type: 'show_create_error', message: error.response.data.errors.full_messages[0] });
//       });
//   };
// };
//
// export const signInToAccount = (email, password) => {
//   return (dispatch) => {
//     signIn(email, password)
//       .then((response) => {
//         const { client, token } = response;
//         dispatch({ type: 'access_account', token, client, email });
//       })
//       .catch((error) => {
//         dispatch({ type: 'show_login_error', message: error.response.data.errors[0] });
//       });
//   };
// };
