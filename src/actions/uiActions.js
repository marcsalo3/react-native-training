export const toggleEye = () => {
  console.log('State toggled!');
  return { type: 'toggle_eye' };
};
export const toggleMenu = () => {
  console.log('State toggled!');
  return { type: 'toggle_menu' };
};
