export const headerStyles = {
  backBtn: {
    backgroundColor: 'transparent',
  },
  sideText: {
    backgroundColor: 'transparent',
    fontSize: 20,
    padding: 10,
  },
  // hamburgerIconContainer: {
  //   paddingRight: 10,
  //   paddingTop: 10
  // },
  // hamburgerIconStyle: {
  //   width: 23,
  //   height: 16
  // },
  container: {
    flex: 1,
    padding: 25,
    flexDirection: 'row'
  },
  textContainer: {
    flex: 3
  },
  textHeadline: {
    fontSize: 25,
    fontWeight: '800',
    textAlign: 'center',
    height: 40
  },
  renderLeft: {
    flex: 1,
    flexDirection: 'column',
  },
  renderRight: {
    flex: 1,
    flexDirection: 'column',
  },
};

export const buttonStyles = {
  button: {
    alignItems: 'center',
    borderRadius: 25,
    padding: 15,
    // shadowColor: '#000',
    // shadowOpacity: 5,
    // shadowRadius: 1,
    // shadowOffset: { width: 4, height: 5 },
    // elevation: 3
  },
  disabled: {
    alignItems: 'center',
    backgroundColor: 'grey',
    borderRadius: 25,
    padding: 15
    // shadowColor: '#000',
    // shadowOpacity: 5,
    // shadowRadius: 1,
    // shadowOffset: { width: 4, height: 5 },
    // elevation: 3,
  },
  text: {
    fontSize: 15,
    fontWeight: 'bold',
    color: '#000'
  },
  textDisabled: {
    fontSize: 15,
    fontWeight: 'bold',
    color: '#fff'
  }
};

export const textfieldStyles = {
  title: {
    fontWeight: 'bold',
    paddingTop: 5
  },
  containerStyle: {
    backgroundColor: '#fff',
    borderColor: '#000',
    borderWidth: 2,
    borderRadius: 5,
    paddingLeft: 5,
    height: 40
  },
  correctStyle: {
    borderWidth: 2,
    borderColor: '#4EE371',
  },
  inputStyle: {
    borderBottomColor: 'transparent',
    justifyContent: 'center',
    paddingVertical: 0,
    paddingTop: 15
  },
  icon: {
    flex: 1,
    textAlign: 'right',
    backgroundColor: 'transparent',
    right: 5,
    top: 8,
    position: 'absolute'
  },
};
