import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image
} from 'react-native';
import { Link } from 'react-router-native';
import { toggleMenu } from '../actions/uiActions';
import { headerStyles } from './commonStyles';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
    };
  }
  renderLeft() {
    if (this.props.leftText === 'Back') {
      return (
        <Link
          to={this.props.leftLink}
          underlayColor="#f0f4f7"
          onPress={() => this.props.leftFunction()}
        >
          <View style={headerStyles.backBtn}>
            <Text>Back</Text>
          </View>
        </Link>
      );
    }
    return (
      <Link
        to={this.props.leftLink}
        underlayColor="#f0f4f7"
        onPress={() => this.props.leftFunction()}
      >
        <Text style={headerStyles.sideText}>{this.props.leftText}</Text>
      </Link>
    );
  }

  renderRight() {
    if (this.props.rightText === 'Menu') {
      return (
        <TouchableOpacity
          onPress={() => this.props.toggleMenu()}
          style={headerStyles.hamburgerIconContainer}
        >
          <Text>Menu</Text>
        </TouchableOpacity>
      );
    }
    return (
      <Link
        to={this.props.rightLink}
        underlayColor="#f0f4f7"
        onPress={() => this.props.rightFunction()}
      >
        <Text style={headerStyles.sideText}>{this.props.rightText}</Text>
      </Link>
    );
  }

  render() {
    const { showMenu } = this.props;
    return (
      <View style={headerStyles.container}>
        <View style={headerStyles.renderLeft}>
          {this.renderLeft()}
        </View>
        <View style={headerStyles.textContainer}>
          <View>
            <Text style={[headerStyles.textHeadline, this.props.extraStyle]}>
              {this.props.headline}
            </Text>
          </View>
        </View>
        <View style={headerStyles.renderRight}>
          {this.renderRight()}
        </View>
      </View>
    );
  }
}

Header.defaultProps = {
  rightFunction: () => null,
  leftFunction: () => null
};
export default connect(null, {
  toggleMenu
})(Header);
