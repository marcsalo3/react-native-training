import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  Text,
  Image,
  TouchableHighlight
} from 'react-native';
import { TextField } from 'react-native-material-textfield';
import { toggleEye } from '../actions/uiActions';
import { textfieldStyles } from './commonStyles';

class NewTextField extends Component {
  renderImage() {
    if (this.props.imageName === 'Eye' && !this.props.correct) {
      return (
        <Text style={textfieldStyles.icon}>
          CheckMark
        </Text>
      );
    }
    if (!this.props.correct) {
      return (
        <Text style={textfieldStyles.icon}>
          CheckMark
        </Text>
      );
    }
    if (this.props.imageName === 'Eye') {
      return (
        <Text
          onPress={() => this.props.toggleEye()}
          //this function is an on press that will call the action, you say which action in login
          style={textfieldStyles.icon}
        >
          Eye
        </Text>
      );
    }
  }
  render() {
    const { showPassword } = this.props;
    return (
      <View>
        <TextField
          label={this.props.label}
          labelPadding={5}
          titleTextStyle={textfieldStyles.title}
          tintColor={'#000'}
          baseColor={'#000'}
          textColor={'#000'}
          secureTextEntry={this.props.seePassword}
          autoCapitalize="none"
          autoCorrect={false}
          error={this.props.error}
          errorColor={this.props.errorColor}
          value={this.props.value}
          labelHeight={0}
          containerStyle={[
            textfieldStyles.containerStyle,
            !this.props.correct ? textfieldStyles.correctStyle : textfieldStyles.containerStyle,
            this.props.containerStyle
          ]}
          inputContainerStyle={[textfieldStyles.inputStyle, this.props.inputStyle]}
          onChangeText={this.props.onChangeText}
          imageName={this.props.imageName}
          multiline={this.props.multiline}
          numberOfLines={this.props.numberOfLines}
        />
        {this.renderImage()}
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  console.log(state);
  const { uiReducer } = state;
  const { showPassword } = uiReducer;
  return {
    showPassword
  };
};

export default connect(mapStateToProps, { toggleEye })(NewTextField);
