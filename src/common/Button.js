import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import { buttonStyles } from './commonStyles';

class CommonButton extends Component {
  render() {
    return (
      <View style={this.props.extraPrimaryStyle}>
        <TouchableOpacity
          style={
            this.props.disabled ? buttonStyles.disabled :
            [buttonStyles.button, { backgroundColor: this.props.backgroundColor }, this.props.extraPrimaryDisabledStyle]
          }
          disabled={this.props.disabled}
          onPress={() => this.props.onClick()}
        >
          <Text style={
              this.props.disabled ? buttonStyles.textDisabled :
              [buttonStyles.text, this.props.extraPrimaryTextStyle]
            }
          >
            {this.props.buttonText}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default CommonButton;
