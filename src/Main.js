import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { NativeRouter, Route, Link } from 'react-router-native';
import Login from './pages/Login';

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: false
    };
  }
  render() {
    return (
      <NativeRouter>
        <View>
          <Login />
        </View>
      </NativeRouter>
    );
  }
}

export default Main;
