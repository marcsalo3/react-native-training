import loginReducer from './loginReducer';
import uiReducer from './uiReducer';

export default {
  loginReducer,
  uiReducer,
};
