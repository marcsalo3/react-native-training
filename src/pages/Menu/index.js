import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Button
} from 'react-native';
import { Link } from 'react-router-native';
import Overlay from 'react-native-modal-overlay';
import { toggleMenu } from '../../actions/uiActions';

class Menu extends Component {
  renderMenu() {
    const { showMenu } = this.props;
    // console.log("Show menu", showMenu);
    return (
      <Overlay
        visible={showMenu}
        closeOnTouchOutside
        animationType="slideInLeft"
        containerStyle={{backgroundColor: 'rgba(37, 8, 10, 0.55)', paddingHorizontal: 0}}
        onClose={() => this.props.toggleMenu()}
        childrenWrapperStyle={{
          height: Dimensions.get('window').height,
          width: Dimensions.get('window').width - 65,
        }}
        animationDuration={500}
        >
        <View style={styles.navWrapper}>
          <View style={styles.nav}>
            <Link
              to="/"
              underlayColor="#f0f4f7"
              style={styles.navItem}
              onPress={() => this.props.toggleMenu()}
            >
              <Text style={styles.navText}>Page One</Text>
            </Link>
            <Link
              to="/my-connections"
              underlayColor="#f0f4f7"
              style={styles.navItem}
              onPress={() => this.props.toggleMenu()}
            >
              <Text style={styles.navText}>Page Two</Text>
            </Link>
            <Link
              to="/new-connections"
              underlayColor="#f0f4f7"
              style={styles.navItem}
              onPress={() => this.props.toggleMenu()}
            >
              <Text style={styles.navText}>Page Three</Text>
            </Link>
            <Link
              to="/messages"
              underlayColor="#f0f4f7"
              style={styles.navItem}
              onPress={() => this.props.toggleMenu()}
            >
              <Text style={styles.navText}>Page Four</Text>
            </Link>
            <Link
              to="/profile"
              underlayColor="#f0f4f7"
              style={styles.navItem}
              onPress={() => this.props.toggleMenu()}
            >
              <Text style={styles.navText}>Settings</Text>
            </Link>
          </View>
        </View>
      </Overlay>
    );
  }

  render() {
    return (
      <View>
        {this.renderMenu()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  navWrapper: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: 20
  },
  nav: {
    padding: 10,
    flex: 1,
    marginBottom: 5,
  },
  navItem: {
    padding: 20,
  },
  navText: {
    fontSize: 18
  }
});

const mapStateToProps = (state) => {
  console.log(state);
  const { uiReducer } = state;
  const { showMenu } = uiReducer;
  return {
    showMenu
  };
};

export default connect(mapStateToProps, { toggleMenu })(Menu);
