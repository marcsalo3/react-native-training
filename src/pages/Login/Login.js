import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import Button from '../../common/Button';
import TextField from '../../common/TextField';
import Header from '../../common/Header';
import Menu from '../Menu';
import { signInToAccount, clearError } from '../../actions/loginActions';
import { toggleEye } from '../../actions/uiActions';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      emailInvalid: true,
      passwordInvalid: true,
      email: '',
      password: ''
    };
  }

  changeEmail(email) {
    this.setState({ email });
    if (email.length > 6) {
      this.setState({ emailInvalid: false });
    } else {
      this.setState({ emailInvalid: true });
    }
  }

  changePassword(password) {
    this.setState({ password });
    if (password.length > 6) {
      this.setState({ passwordInvalid: false });
    } else {
      this.setState({ passwordInvalid: true });
    }
  }

  render() {
    const { showPassword } = this.props;
    return (
      <ScrollView style={{padding: 25}}>
        <Header
          headline="Login"
          leftLink="/"
          leftText="Back"
          rightLink="/"
          rightText="Menu"
        />
        <View>
          <View style={{paddingTop: 50}}>
            <TextField
              label="Enter Your Email Address"
              value={this.state.email}
              correct={this.state.emailInvalid}
              onChangeText={(email) => this.changeEmail(email)}
            />
          </View>
          <View style={{paddingTop: 50}}>
            <TextField
              label="Enter Your Password"
              value={this.state.password}
              correct={this.state.passwordInvalid}
              onChangeText={(password) => this.changePassword(password)}
              imageName="Eye"
              seePassword={showPassword}
              toggleEye={() => this.props.toggleEye()}
            />
          </View>
          <Menu />
          <Text style={{padding: 5, paddingTop: 15, fontWeight: 'bold', textAlign: 'center'}}>
            Reusable Components Instructions
          </Text>
          <Text style={{padding: 5}}>
            Common Components include: Button, Header, Menu and TextField. All component styles are in common/commonStyles.
            All common components also have optional additional styling available through props for custom styling.
            Button background color can be applied through props. Other colors will need to be added to commonStyles
            file where appropriate.
          </Text>
          <Text style={{padding: 5}}>
            This login screen has basic validation in each TextField. The login reducer, actions, and api connections
            are already in place. They are commented out to reduce errors. Once api is set up, uncomment loginActions,
            loginReducer files, and the bottom of the login file.
            To add icons to Login TextFields follow installation instructions here: github.com/oblador/react-native-vector-icons
          </Text>
          <Text>
            Icons can be added into the Header where text Chevron and Menu are currently. Styling for those icons can be found
            in commonStyles under hamburgerIconStyle and hamburgerIconContainer which are commented out. There is also shadow
            styling available for the button which is commented out.
          </Text>
          <View style={{paddingBottom: 30}}>
            <Button
              buttonText="Sign In"
              disabled={this.state.emailInvalid || this.state.passwordInvalid}
              onClick={() => console.log('Signed In')}
              backgroundColor={'grey'}
              extraPrimaryStyle={{
                paddingTop: 50
              }}
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}
const mapStateToProps = ({ loginReducer, uiReducer }) => {
  // const { loggedIn, loginError } = loginReducer;
  const { showPassword } = uiReducer;
  return {
    // loggedIn,
    // loginError,
    showPassword
  };
};

export default connect(mapStateToProps, {
  // signInToAccount,
  // clearError,
  toggleEye
})(Login);
