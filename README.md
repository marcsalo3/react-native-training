# NuRelm React Native Starter
## About this repo

This repo is the starting point for all React Native projects we develop. It includes:

+ React
+ React Native
+ Redux
+ React Redux
+ Redux Persist
+ React Native Router
# How to use this repository

+ Create the project new repo in bitbucket
+ Clone this repository
+ cd to your local copy of the old repo you want to extract from, which is set up to track the specific-branch branch that will become the new-repo's master.
+ $ git push https://bitbucket.org/nurelmdevelopers/new-repo.git +specific-branch:master (e.g.: git push https://bitbucket.org/nurelmdevelopers/sweet-new-react-project.git +master:master)

# In your new project

+ Edit starter app name in `index.js`, `package.json`, `app.json` to be that of the new project
+ Run `npm install`
+ Run `react-native eject` to create ios and android directories
+ Run `react-native link`
+ Run `react-native run-ios` to simulate.



